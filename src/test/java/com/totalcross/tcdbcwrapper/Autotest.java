package com.totalcross.tcdbcwrapper;

import java.sql.SQLException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import totalcross.sys.Convert;
import totalcross.sys.Settings;

public class Autotest {
	
	private static ConnectionWrapper connWrapped;
	
	@BeforeClass
	public static void initTC() {
		Convert.normalizePath("/tmp");
	}
	
	@AfterClass
	public static void closeConn() throws SQLException {
		ConnectionWrapper conn = getConn();
		
		if (conn != null) {
			conn.close();
			
			conn = null; // forces reopening if needed
		}
	}
	
	
	@Test
	public void rodaConsulta() throws SQLException {
		
		try (StatementWrapper stmt = getConn().createStatement()) {
			stmt.execute("CREATE TABLE IF NOT EXISTS test (id AUTO INCREMENT PRIMARY KEY, desc)");
		}
	}



	private static ConnectionWrapper getConn() throws SQLException {
		if (connWrapped == null) {
			String dirPath = ((Settings.dataPath != null && !"".equals(Settings.dataPath))? Settings.dataPath + "/" : "");
			String filePath = dirPath + "test.db";
			
			connWrapped = ConnectionWrapper.getConnectionWrapper(totalcross.sql.DriverManager.getConnection("jdbc:sqlite:" + filePath));
		}
		return connWrapped;
	}
}
