package com.totalcross.tcdbcwrapper;
import java.sql.SQLException;
import java.sql.SQLWarning;

import totalcross.sql.ResultSet;
import totalcross.sql.ResultSetMetaData;
import totalcross.sql.Timestamp;
import totalcross.sys.Time;
import totalcross.sys.Vm;
import totalcross.util.BigDecimal;
import totalcross.util.Date;

public class ResultSetWrapper implements ResultSet, AutoCloseable {
	final ResultSet rs;
	public final int id;
	
	public static ResultSetWrapper getResultSetWrapper(ResultSet rs) {
		if (rs instanceof ResultSetWrapper) {
			return (ResultSetWrapper) rs;
		} else {
			return new ResultSetWrapper(rs);
		}
	}
	
	public ResultSetWrapper(ResultSet rs) {
		this.rs = rs;
		id = IdManager.getId();
		
		Vm.debug("Created ResultSet id " + id);
	}
	
	@Override
	public boolean absolute(int arg0) throws SQLException {
		return rs.absolute(arg0);
	}

	@Override
	public void afterLast() throws SQLException {
		rs.afterLast();
	}

	@Override
	public void beforeFirst() throws SQLException {
		rs.beforeFirst();
	}

	@Override
	public void clearWarnings() throws SQLException {
		rs.clearWarnings();
	}

	@Override
	public void close() throws SQLException {
		rs.close();
		
		Vm.debug("Closed ResultSet id " + id);
	}

	@Override
	public int findColumn(String arg0) throws SQLException {
		return rs.findColumn(arg0);
	}

	@Override
	public boolean first() throws SQLException {
		return rs.first();
	}

	@Override
	public BigDecimal getBigDecimal(int arg0) throws SQLException {
		return rs.getBigDecimal(arg0);
	}

	@Override
	public BigDecimal getBigDecimal(String arg0) throws SQLException {
		return rs.getBigDecimal(arg0);
	}

	@Override
	public BigDecimal getBigDecimal(int arg0, int arg1) throws SQLException {
		return rs.getBigDecimal(arg0, arg1);
	}

	@Override
	public BigDecimal getBigDecimal(String arg0, int arg1) throws SQLException {
		return rs.getBigDecimal(arg0, arg1);
	}

	@Override
	public boolean getBoolean(int arg0) throws SQLException {
		return rs.getBoolean(arg0);
	}

	@Override
	public boolean getBoolean(String arg0) throws SQLException {
		return rs.getBoolean(arg0);
	}

	@Override
	public byte getByte(int arg0) throws SQLException {
		return rs.getByte(arg0);
	}

	@Override
	public byte getByte(String arg0) throws SQLException {
		return rs.getByte(arg0);
	}

	@Override
	public byte[] getBytes(int arg0) throws SQLException {
		return rs.getBytes(arg0);
	}

	@Override
	public byte[] getBytes(String arg0) throws SQLException {
		return rs.getBytes(arg0);
	}

	@Override
	public int getConcurrency() throws SQLException {
		return rs.getConcurrency();
	}

	@Override
	public String getCursorName() throws SQLException {
		return rs.getCursorName();
	}

	@Override
	public Date getDate(int arg0) throws SQLException {
		return rs.getDate(arg0);
	}

	@Override
	public Date getDate(String arg0) throws SQLException {
		return rs.getDate(arg0);
	}

	@Override
	public double getDouble(int arg0) throws SQLException {
		return rs.getDouble(arg0);
	}

	@Override
	public double getDouble(String arg0) throws SQLException {
		return rs.getDouble(arg0);
	}

	@Override
	public int getFetchDirection() throws SQLException {
		return rs.getFetchDirection();
	}

	@Override
	public int getFetchSize() throws SQLException {
		return rs.getFetchSize();
	}

	@Override
	public int getInt(int arg0) throws SQLException {
		return rs.getInt(arg0);
	}

	@Override
	public int getInt(String arg0) throws SQLException {
		return rs.getInt(arg0);
	}

	@Override
	public long getLong(int arg0) throws SQLException {
		return rs.getLong(arg0);
	}

	@Override
	public long getLong(String arg0) throws SQLException {
		return rs.getLong(arg0);
	}

	@Override
	public ResultSetMetaData getMetaData() throws SQLException {
		return rs.getMetaData();
	}

	@Override
	public Object getObject(int arg0) throws SQLException {
		return rs.getObject(arg0);
	}

	@Override
	public Object getObject(String arg0) throws SQLException {
		return rs.getObject(arg0);
	}

	@Override
	public int getRow() throws SQLException {
		return rs.getRow();
	}

	@Override
	public short getShort(int arg0) throws SQLException {
		return rs.getShort(arg0);
	}

	@Override
	public short getShort(String arg0) throws SQLException {
		return rs.getShort(arg0);
	}

	@Override
	public StatementWrapper getStatement() throws SQLException {
		return StatementWrapper.getStatementWrapper(rs.getStatement());
	}

	@Override
	public String getString(int arg0) throws SQLException {
		return rs.getString(arg0);
	}

	@Override
	public String getString(String arg0) throws SQLException {
		return rs.getString(arg0);
	}

	@Override
	public Time getTime(int arg0) throws SQLException {
		return rs.getTime(arg0);
	}

	@Override
	public Time getTime(String arg0) throws SQLException {
		return rs.getTime(arg0);
	}

	@Override
	public Timestamp getTimestamp(int arg0) throws SQLException {
		return rs.getTimestamp(arg0);
	}

	@Override
	public Timestamp getTimestamp(String arg0) throws SQLException {
		return rs.getTimestamp(arg0);
	}

	@Override
	public int getType() throws SQLException {
		return rs.getType();
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		return rs.getWarnings();
	}

	@Override
	public boolean isAfterLast() throws SQLException {
		return rs.isAfterLast();
	}

	@Override
	public boolean isBeforeFirst() throws SQLException {
		return rs.isBeforeFirst();
	}

	@Override
	public boolean isFirst() throws SQLException {
		return rs.isFirst();
	}

	@Override
	public boolean isLast() throws SQLException {
		return rs.isLast();
	}

	@Override
	public boolean last() throws SQLException {
		return rs.last();
	}

	@Override
	public boolean next() throws SQLException {
		return rs.next();
	}

	@Override
	public boolean previous() throws SQLException {
		return rs.previous();
	}

	@Override
	public boolean relative(int arg0) throws SQLException {
		return rs.relative(arg0);
	}

	@Override
	public void setFetchDirection(int arg0) throws SQLException {
		rs.setFetchDirection(arg0);
	}

	@Override
	public void setFetchSize(int arg0) throws SQLException {
		rs.setFetchSize(arg0);
	}

	@Override
	public boolean wasNull() throws SQLException {
		return rs.wasNull();
	}

}
